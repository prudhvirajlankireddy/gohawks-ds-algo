package org.gohawks.algos;

public class CountingValleys {

	public static void main(String[] args) {
		String input1 = "UDDDUDUU";
		System.out.println("Number of Valleys in the Hike : "+ input1 + " is " + countValleys(8, input1));
		String input2 = "DUDDUUUUDDD";
		System.out.println("Number of Valleys in the Hike : "+ input2 + " is " + countValleys(11, input2));
	}
	
	/**
	 * Time complexity is O(N) - N is number of steps
	 * Space Complexity is O(1)
	 * @param numberOfSteps
	 * @param steps
	 * @return
	 */
	
	public static int countValleys(int numberOfSteps, String steps) {
		int numberOfValleys = 0; 
		char[] stepsArray = (steps != "" || steps != null) ? steps.toCharArray() : null;
		
		if(stepsArray.length != numberOfSteps) {
			numberOfValleys = -1; 
		}
		int n  = 0;
		int rollingcount = 0;
		while(n < numberOfSteps) {
		  rollingcount += stepsArray[n] == 'U' ? 1: -1;
		  if(stepsArray[n] == 'U' && rollingcount ==0) {
			  numberOfValleys++;
		  }
		  n++;
		}
		return numberOfValleys;
	}
}
