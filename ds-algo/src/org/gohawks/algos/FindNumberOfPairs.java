package org.gohawks.algos;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author Prudhvi
 * 
 * 
 * Question : Count matching pairs in an array 
 *           Input : int[]
 *           Output : int 
 *           
 *           Example 1 : Input : 5, 10, 10 Output : 1
 *           Example 2 : Input : 10, 20, 20, 20, 30, 40, 50, 30, 10 -- 3   
 *           
 */
public class FindNumberOfPairs {

	public static int countOfPairs(int[] array) {
		int count=0;
		
		Map<Integer, Integer> mapOfCounts = new HashMap<Integer, Integer>();
		for(int i=0; i < array.length ; i++){
			if(mapOfCounts.get(array[i]) == null) {
				mapOfCounts.put(array[i],1);
			} else {
				int currentCount = mapOfCounts.get(array[i]);
				currentCount=currentCount+1;
				mapOfCounts.put(array[i],currentCount);
			}
		}
		
		for(Integer key : mapOfCounts.keySet()) {
			count = count + (mapOfCounts.get(key)> 1 ? (int) Math.floor(mapOfCounts.get(key)/2): 0);
		}
		
		return count;
	}
	
	
	public static void main(String[] args) {
		//int[] input = new int[] {10,20,29,20,20,20,30,50};
		int[] input = new int[] {10,1,5};
		System.out.println(countOfPairs(input));
	}

}
